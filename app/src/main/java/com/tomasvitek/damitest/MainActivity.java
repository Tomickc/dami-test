package com.tomasvitek.damitest;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.tomasvitek.damitest.UI.splash.SplashscreenFragment;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.Contact;
import com.tomasvitek.damitest.models.MapMarker;
import com.tomasvitek.damitest.models.User;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.tool_bar)
    public Toolbar toolbar;
    public List<MapMarker> mapPointsList;
    public List<Contact> contacts,contactsSearch;
    public User user;
    public int contactPosition;
    public Contact contact;
    public MapMarker mapMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ButterKnife.bind(this);

        //Setting the Toolbar
        setSupportActionBar(toolbar);


        //Show Splashscreen
        showSplash();

        //Set ImageLoader
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .diskCacheExtraOptions(480, 320, null)
                .threadPoolSize(3)
                .build();
        ImageLoader.getInstance().init(config);
    }


    private void showSplash() {
        hideToolbar();

        //Open new fragment
        showFragment(SplashscreenFragment.newInstance(), SplashscreenFragment.TAG, false);
    }

    private void hideToolbar() {
        //Hide created Toolbar (While showing splashscreen)
        getSupportActionBar().hide();
    }

    public void showFragment(BaseFragment fragment, String tag, boolean backStack) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        //Replace fragment
        transaction.replace(R.id.fragment, fragment, tag);
        if (backStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    //Save pointsList to MainActivity
    public void setMapPointsList(List<MapMarker> mapPointsList) {
        //Save mapPoints
        this.mapPointsList = mapPointsList;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public void setContactsSearch(List<Contact> contactsSearch) {
        this.contactsSearch = contactsSearch;
    }


    //Get custom toolbar
    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    //Save mapMarker to MainActivity
    public void setMapMarker(MapMarker mapMarker) {
        this.mapMarker = mapMarker;
    }

    //Save user to MainActivity
    public void setUser(User user) {
        this.user = user;
    }

    public void setContactPosition(int contactPosition) {
        this.contactPosition = contactPosition;
    }
}

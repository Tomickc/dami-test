package com.tomasvitek.damitest.models;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by Tomáš Vítek on 12.03.2016.
 */


//Class will show Map point info like position or title and description
public class MapMarker {

    //Define variables and Objects
    double lat, lng;
    String desc, title;
    List<String> photo;
    Bitmap infoViewBitmap;

    //Get bitmap which will show point image
    public Bitmap getInfoViewBitmap() {
        return infoViewBitmap;
    }

    //Set bitmap which will show point image
    public void setInfoViewBitmap(Bitmap infoViewBitmap) {
        this.infoViewBitmap = infoViewBitmap;
    }

    //Get List of photo urls
    public List<String> getPhotos() {
        return photo;
    }

    //Get point description
    public String getDesc() {
        return desc;
    }

    //Get point title
    public String getTitle() {
        return title;
    }

    //Get first parameter of coordinate
    public double getLat() {
        return lat;
    }

    //Get second parameter of coordinate
    public double getLng() {
        return lng;
    }
}


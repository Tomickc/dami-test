package com.tomasvitek.damitest.models;

import java.util.List;

//class will show info about logged user
public class User {

    //define variables and objects
    int id;
    String  name, lastname, photo, token;
    List<String> properties;
    List<MapMarker> favorites;

    //get user's id
    public int getId() {
        return id;
    }


    //get user's name
    public String getName() {
        return name;
    }

    //get user's surname
    public String getLastname() {
        return lastname;
    }

    //get user's profile photo
    public String getPhoto() {
        return photo;
    }

    //get user's toke
    public String getToken() {
        return token;
    }

    //get user's list of properties
    public List<String> getProperties() {
        return properties;
    }

    //get user's list of favorite points on Map
    public List<MapMarker> getFavorites() {
        return favorites;
    }
}

package com.tomasvitek.damitest.models;

import android.graphics.Bitmap;

/**
 * Created by Tomáš Vítek on 05.04.2016 at 20:38.
 */

//Class will show Contact info like name, lastname or email and number
public class Contact {

    //Define variables and objects
    int id;
    String email, name, lastname, photo, phone;
    Bitmap contactBitmap;

    //Get Contact's ID
    public int getId() {
        return id;
    }

    //Get Contact's email
    public String getEmail() {
        return email;
    }

    //Get Contact's name
    public String getName() {
        return name;
    }

    //Get Contact's surname
    public String getLastname() {
        return lastname;
    }

    //Get Contact's phone number
    public String getPhone() {
        return phone;
    }

    //Get Contact's photo
    public String getPhoto() {
        return photo;
    }


    //Get Contact's bitmapImage
    public Bitmap getContactBitmap() {
        return contactBitmap;
    }

    //Set Contact's bitmapImage
    public void setContactBitmap(Bitmap contactBitmap) {
        this.contactBitmap = contactBitmap;
    }
}

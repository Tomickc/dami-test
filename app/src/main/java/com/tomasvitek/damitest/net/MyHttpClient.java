package com.tomasvitek.damitest.net;

import com.loopj.android.http.*;

public class MyHttpClient {

    //Download JSON info from BASE_URL
    private static final String BASE_URL = "http://androidtest.dev.damidev.com/api/";

    //new Instance of AsyncHttpClient
    private static AsyncHttpClient client = new AsyncHttpClient();

    //Method to GET infos from JSON
    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.setTimeout(30000);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    //Method to POST parameters and get infos from JSON
    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    //Add JSON method with infos to BASE_URL
    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
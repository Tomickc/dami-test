package com.tomasvitek.damitest.UI.maps;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.MapMarker;

import butterknife.OnClick;

/**
 * Created by Tomáš Vítek on 29.03.2016 at 19:35.
 */
public class MarkerDetailFragment extends BaseFragment {

    public static final String TAG = "marker_detail_fragment";
    int imgposition = 0;
    ImageView images;
    MapMarker mapMarker;
    TextView title, description;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        images = (ImageView) view.findViewById((R.id.image_detail));
        title = (TextView) view.findViewById((R.id.titleTV));
        description = (TextView) view.findViewById((R.id.description));

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Set toolbar title
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Detail");

        //More toolbar settings
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((MainActivity) getActivity()).getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back when arrow is clicked
                getFragmentManager().popBackStack();
            }
        });

        //get saved mapMarker from MainActivity
        mapMarker = ((MainActivity) getActivity()).mapMarker;

        //Set imageLoader
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(mapMarker.getPhotos().get(imgposition), images);
        //Display some infos about point
        title.setText(mapMarker.getTitle());
        description.setText(mapMarker.getDesc());
    }

    @OnClick(R.id.image_detail)
    void onImageClick() {
        if (imgposition < mapMarker.getPhotos().size() - 1) {
            imgposition++;
        } else imgposition = 0;

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.loadImage(mapMarker.getPhotos().get(imgposition), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                //Save infoViewBitmap to MainActivity
                Bitmap scaled = Bitmap.createScaledBitmap(loadedImage, 600, 300, true);
                images.setImageBitmap(scaled);
            }
        });
    }

    //Fragment's newInstance method
    public static MarkerDetailFragment newInstance() {
        MarkerDetailFragment fragment = new MarkerDetailFragment();
        return fragment;
    }
}

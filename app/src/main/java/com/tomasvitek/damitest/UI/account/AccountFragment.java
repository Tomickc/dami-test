package com.tomasvitek.damitest.UI.account;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.Contact;
import com.tomasvitek.damitest.models.User;
import com.tomasvitek.damitest.net.MyHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Tomáš Vítek on 29.03.2016 at 19:35.
 */
public class AccountFragment extends BaseFragment {

    //Set fragment's TAG
    public static final String TAG = "account_fragment";

    String propertiesString;

    @Bind(R.id.image_detail)ImageView userImage;
    @Bind(R.id.titleTV)TextView name;
    @Bind(R.id.surnameTV)TextView surname;
    @Bind(R.id.description)TextView properties;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //Set custom layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //NavigationDrawer setup
        showNavigationDrawer();

        //Set toolbar title
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Account");

        //Get user class which is saved in MainActivity
        User user = ((MainActivity) getActivity()).user;


        Log.d("Token",user.getToken());
        //POST method with token parameter to get User's contacts
        MyHttpClient.post("getContacts", new RequestParams("token", user.getToken()), new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                //Treat with try/catch
                try {

                    //create new instance of GSON
                    Gson gson = new Gson();

                    //Array of MapMarkers
                    JSONArray responses = response.getJSONArray("response");
                    List<Contact> contacts = new ArrayList<Contact>();

                    //Get all contacts
                    for (int i = 0; i < responses.length(); i++) {

                        //get current contact
                        final Contact contact = gson.fromJson(responses.getJSONObject(i).toString(), Contact.class);

                        //Set new imageLoader
                        ImageLoader imageLoader = ImageLoader.getInstance();
                        imageLoader.loadImage(contact.getPhoto(), new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


                                //Save contactBitmap to MainActivity
                                contact.setContactBitmap(loadedImage);
                            }
                        });

                        //add mapMarker to List
                        contacts.add(contact);
                    }

                    //Save contacts list to MainActivity
                    ((MainActivity) getActivity()).setContacts(contacts);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });

        //set infos from user class
        name.setText(user.getName());
        surname.setText(user.getLastname());

        //Check if properties aren't null
        if (user.getProperties() != null) {
            //set properties text
            for (int i = 0; i < user.getProperties().size(); i++) {
                propertiesString += user.getProperties().get(i);
                propertiesString += "\n";
                properties.setText(propertiesString);
            }
        } else properties.setText("Properties are empty");

        //set profile image
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(user.getPhoto(), userImage);

    }

    //Fragment's newInstance method
    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }

}

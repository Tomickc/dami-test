package com.tomasvitek.damitest.UI.contact;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.Contact;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by Tomáš Vítek on 05.04.2016 at 18:23.
 */
public class ContactsFragment extends BaseFragment {

    //Set Fragment's TAG
    public static final String TAG = "contacts_fragment";

    //Define views and objects
    ListView listView;
    List<String> names;
    List<Bitmap> contactBitmaps;

    @Bind(R.id.contacts_search)EditText searcher;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Enable icons in toolbar
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Set custom layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        //Declare views
        listView = (ListView) view.findViewById(R.id.contacts_list);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //More toolbar settings
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        //Set toolbar title
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Contacts");

        //Show NavigationDrawer
        showNavigationDrawer();

        //declare lists
        names = new ArrayList<String>();
        contactBitmaps = new ArrayList<Bitmap>();

        //add all contacts into list
        for (int i = 0; i < ((MainActivity) getActivity()).contacts.size(); i++) {
            Contact contact = ((MainActivity) getActivity()).contacts.get(i);
            names.add(contact.getName() + " " + contact.getLastname());
            contactBitmaps.add(contact.getContactBitmap());
        }

        //Declare new Adapter
        final ContactsAdapter contactsAdapter = new ContactsAdapter(getActivity(), names, contactBitmaps);

        //Edittext which will search contacts
        searcher.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //Create new contacts list
                List<Contact> contacts = new ArrayList<Contact>();

                //get searched word's lenght
                int textlength = searcher.getText().length();

                //clear all lists and adapter
                contactsAdapter.clear();
                names.clear();
                contactBitmaps.clear();

                //get all contacts
                for (int i = 0; i < ((MainActivity) getActivity()).contacts.size(); i++) {
                    Contact contact = ((MainActivity) getActivity()).contacts.get(i);
                    String name = contact.getName() + " " + contact.getLastname();

                    //check if searching word is smaller than current contact
                    if (textlength <= name.length()) {

                        //check if searching word equals current contact
                        if (searcher.getText().toString().equalsIgnoreCase((String) name.subSequence(0, textlength))) {

                            //add searched contact to names
                            names.add(name);

                            //add contact's image to list
                            contactBitmaps.add(contact.getContactBitmap());

                            //add contact to contacts
                            contacts.add(contact);
                        }
                    }
                }

                //save list of contacts to MainActivity
                ((MainActivity) getActivity()).setContactsSearch(contacts);

                //create new adapter with searched contacts
                final ContactsAdapter contactsAdapter = new ContactsAdapter(getActivity(), names, contactBitmaps);

                //set adapter into listview
                listView.setAdapter(contactsAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        //Save searched contact to MainActivity
                        ((MainActivity) getActivity()).setContact(((MainActivity) getActivity()).contactsSearch.get(position));
                        ((MainActivity) getActivity()).setContactPosition(position);
                        showFragment(ContactDetailFragment.newInstance(), ContactDetailFragment.TAG, true);
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //set adapter into listview
        listView.setAdapter(contactsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Save contact to MainActivity
                ((MainActivity) getActivity()).setContact(((MainActivity) getActivity()).contacts.get(position));
                ((MainActivity) getActivity()).setContactPosition(position);
                showFragment(ContactDetailFragment.newInstance(), ContactDetailFragment.TAG, true);
            }
        });

    }

    //Fragment's newInstance method
    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        return fragment;
    }


    //Show add icon in toolbar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_contacts, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Add icon click
        if (id == R.id.add) {
            showFragment(ContactAddFragment.newInstance(), ContactAddFragment.TAG, true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

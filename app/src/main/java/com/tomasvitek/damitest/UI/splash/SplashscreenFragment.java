package com.tomasvitek.damitest.UI.splash;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.UI.login.LoginFragment;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.MapMarker;
import com.tomasvitek.damitest.net.MyHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Tomáš Vítek on 08.03.2016.
 */

public class SplashscreenFragment extends BaseFragment {

    //Set fragment's TAG
    public static final String TAG = "splashscreen_fragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Set custom layout for this fragment
        View view = inflater.inflate(R.layout.fragment_splashscreen, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            //Download API info
            getPublicTimeline();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Show splashscreen for 2 seconds
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                showFragment(LoginFragment.newInstance(), LoginFragment.TAG, false);
                ((AppCompatActivity) getActivity()).getSupportActionBar().show();
            }
        }, 2000);
    }

    //Fragment's newInstance method
    public static SplashscreenFragment newInstance() {
        SplashscreenFragment fragment = new SplashscreenFragment();
        return fragment;
    }

    public void getPublicTimeline() throws JSONException {

        //Api page
        MyHttpClient.get("getPointsOnMap", null, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    Gson gson = new Gson();

                    //Array of MapMarkers
                    JSONArray responses = response.getJSONArray("response");
                    List<MapMarker> points = new ArrayList<MapMarker>();

                    for (int i = 0; i < responses.length(); i++) {
                        //get MapMarker's position
                        final MapMarker mapMarker = gson.fromJson(responses.getJSONObject(i).toString(), MapMarker.class);

                        //Set new imageLoader
                        ImageLoader imageLoader = ImageLoader.getInstance();
                        imageLoader.loadImage(mapMarker.getPhotos().get(0), new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                //Save infoViewBitmap to MainActivity
                                mapMarker.setInfoViewBitmap(loadedImage);
                            }
                        });

                        //add mapMarker to List
                        points.add(mapMarker);
                    }

                    //Save List of MapMarker to MainActivity
                    ((MainActivity) getActivity()).setMapPointsList(points);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });
    }
}

package com.tomasvitek.damitest.UI.contact;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tomasvitek.damitest.R;

import java.util.List;

/**
 * Created by Tomáš Vítek on 06.04.2016 at 11:37.
 */

//Custom adapter with my own layout
public class ContactsAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final List<String> names;
    private final List<Bitmap> images;

    public ContactsAdapter(Activity context, List<String> names, List<Bitmap> images) {
        super(context, R.layout.list_item_contacts, names);

        this.context = context;
        this.names = names;
        this.images = images;
    }

    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item_contacts, null, true);

        TextView nameTV = (TextView) rowView.findViewById(R.id.contact_text);
        ImageView accountIMG = (ImageView) rowView.findViewById(R.id.contact_image);

        nameTV.setText(names.get(position));
        accountIMG.setImageBitmap(images.get(position));

        return rowView;
    }
}
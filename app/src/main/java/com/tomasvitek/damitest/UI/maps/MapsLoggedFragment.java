package com.tomasvitek.damitest.UI.maps;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.MapMarker;
import com.tomasvitek.damitest.models.User;

import java.util.List;

public class MapsLoggedFragment extends BaseFragment implements OnMapReadyCallback {

    //Set Fragment's TAG
    public static final String TAG = "maps_logged_fragment";

    GoogleMap mMap;
    ImageView image;
    List<MapMarker> points;

    private static View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_maps_logged, container, false);
        } catch (InflateException e) {
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Set toolbar config
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        showNavigationDrawer();

        //Set Map for fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_logged);
        mapFragment.getMapAsync(this);

        //Set toolbar title
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Map");
    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;

        //Map zoom
        mMap.moveCamera(CameraUpdateFactory.zoomTo(2));

        //get points saved in MainActivity
        points = ((MainActivity) getActivity()).mapPointsList;

        //get user class saved in MainActivity
        User user = ((MainActivity) getActivity()).user;

        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);

        //check if user wants to show only favorites points
        if (sharedPreferences.getBoolean("showOnlyFavorites", false)) {

            //Show only favorites points
            for (int i = 0; i < user.getFavorites().size(); i++) {
                MapMarker mapMarker = user.getFavorites().get(i);
                addMarker(mapMarker.getLat(), mapMarker.getLng());
            }
        } else {

            //Show all points
            for (int i = 0; i < points.size(); i++) {
                MapMarker mapMarker = points.get(i);
                addMarker(mapMarker.getLat(), mapMarker.getLng());
            }
        }

        //Set pop-up window
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = getActivity().getLayoutInflater().inflate(R.layout.map_info, null);
                image = (ImageView) v.findViewById(R.id.place_image);


                for (int i = 0; i < points.size(); i++) {
                    MapMarker mapMarker = points.get(i);


                    //Check if clicked point is current mapMarker
                    if (marker.getPosition().equals(new LatLng(mapMarker.getLat(), mapMarker.getLng()))) {

                        //Save current mapMarker to MainActivity
                        ((MainActivity) getActivity()).setMapMarker(mapMarker);

                        //Set infoBitmap
                        image.setImageBitmap(mapMarker.getInfoViewBitmap());

                        //on Image click show markerDetailFragment
                        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                showFragment(MarkerDetailFragment.newInstance(), MarkerDetailFragment.TAG, true);
                            }
                        });
                    }
                }
                return v;
            }
        });
    }

    //Fragment's newInstance method
    public static MapsLoggedFragment newInstance() {
        MapsLoggedFragment fragment = new MapsLoggedFragment();
        return fragment;
    }

    //Method which will add points to map
    public Marker addMarker(double lat, double lng) {
        return mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).icon((BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))));
    }


    //Show filter icon in toolbar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_map, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Filter icon click
        if (id == R.id.filter) {

            //checkBox text
            final CharSequence[] items = {"Zobrazit pouze oblíbené"};

            //Create new dialog with checkBox
            AlertDialog dialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Filtr")
                    .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                            SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();

                            //if is checkBox checked put boolean true to sharedPrefs
                            //else add false
                            if (isChecked) {
                                editor.putBoolean("showOnlyFavorites", true);
                            } else {
                                editor.putBoolean("showOnlyFavorites", false);
                            }
                            //commit
                            editor.apply();

                            //positive button with text "OK"
                        }
                    }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //Will refresh current fragment
                            showFragment(MapsLoggedFragment.newInstance(), MapsLoggedFragment.TAG, false);
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //dismiss dialog
                            dialog.dismiss();
                        }
                    }).create();

            //Show dialog
            dialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

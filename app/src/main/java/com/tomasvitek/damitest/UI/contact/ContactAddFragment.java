package com.tomasvitek.damitest.UI.contact;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.Contact;
import com.tomasvitek.damitest.models.User;
import com.tomasvitek.damitest.net.MyHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.Bind;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Tomáš Vítek on 29.03.2016 at 19:35.
 */
public class ContactAddFragment extends BaseFragment {

    //Set fragment's TAG
    public static final String TAG = "contact_add_fragment";

    //Load image constant
    private static final int RESULT_LOAD_IMG = 1;

    //Define objects
    @Bind(R.id.edit_image) ImageView image;


    @Bind(R.id.edit_name)
    EditText name;

    @Bind(R.id.edit_surname) EditText surname;
    @Bind(R.id.edit_number) EditText number;
    @Bind(R.id.edit_email) EditText email;

    File fileImage;
    Contact contact;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Enable icons in toolbar
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //Set fragment's view
        View view = inflater.inflate(R.layout.fragment_contact_edit_add, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Set toolbar title
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Add contact");

        //More toolbar settings
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((MainActivity) getActivity()).getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back when arrow is clicked
                getFragmentManager().popBackStack();
            }
        });


        //Get contact from MainActivity
        contact = ((MainActivity) getActivity()).contact;

        //Pick image from galleri
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
            }
        });
    }

    //Method called when image is selected
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == -1 && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String path = cursor.getString(columnIndex);
                cursor.close();

                int file_size = Integer.parseInt(String.valueOf(new File(path).length() / 1024));
                Log.d("File type", getMimeType(path));

                Log.d("IMG","size: "+file_size+" kB");
                //Check if image has correct size and type
                if (file_size <= 400 && (getMimeType(path).contains("jpeg") || getMimeType(path).contains("jpg") || getMimeType(path).contains("png") || getMimeType(path).contains("gif"))) {


                    //Set selected bitmap to imageView
                    image.setImageBitmap(BitmapFactory.decodeFile(path));

                    Toast.makeText(getActivity(), "File created", Toast.LENGTH_SHORT).show();

                    //Create new File
                    fileImage = new File(path);
                } else {
                    Toast.makeText(getActivity(), "Photo is too large or is bad type", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }

    //Get image Type
    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    //Fragment's newInstance method
    public static ContactAddFragment newInstance() {
        ContactAddFragment fragment = new ContactAddFragment();
        return fragment;
    }

    //Show Done icon in toolbar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_contacts_edit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Done icon click
        if (id == R.id.done) {

            //Check if name isnt empty
            if (!name.getText().toString().equals("")) {

                //Get user class from MainActivity
                User user = ((MainActivity) getActivity()).user;

                //Put params to POST method
                RequestParams requestParams = new RequestParams();
                requestParams.put("token", user.getToken());
                requestParams.put("name", name.getText().toString());
                requestParams.put("lastname", surname.getText().toString());
                requestParams.put("phone", number.getText().toString());
                requestParams.put("email", email.getText().toString());

                //Try to save image
                try {
                    requestParams.put("photo", fileImage);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                //POST method which will add contact
                MyHttpClient.post("addContact", requestParams, new JsonHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        Toast.makeText(getActivity(), "Save failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Toast.makeText(getActivity(), "Saved", Toast.LENGTH_SHORT).show();

                        //Treat with try/catch
                        try {

                            Gson gson = new Gson();

                            final Contact contact = gson.fromJson(response.getJSONObject("response").toString(), Contact.class);

                            //Set new imageLoader
                            ImageLoader imageLoader = ImageLoader.getInstance();
                            imageLoader.loadImage(contact.getPhoto(), new SimpleImageLoadingListener() {
                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                                    //Save contact's image to MainActivity
                                    contact.setContactBitmap(loadedImage);
                                }
                            });

                            //add contact to List
                            ((MainActivity)getActivity()).contacts.add(contact);

                            //Save contacts to MainActivity
                            ((MainActivity)getActivity()).setContacts(((MainActivity)getActivity()).contacts);

                            showFragment(ContactsFragment.newInstance(),ContactsFragment.TAG,false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("Saving","Err");
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                    }
                });

                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}

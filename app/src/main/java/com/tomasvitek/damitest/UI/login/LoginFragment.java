package com.tomasvitek.damitest.UI.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.UI.account.AccountFragment;
import com.tomasvitek.damitest.UI.maps.MapsFragment;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.User;
import com.tomasvitek.damitest.net.MyHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Tomáš Vítek on 07.03.2016 at 11:09.
 */

public class LoginFragment extends BaseFragment implements Validator.ValidationListener {

    SharedPreferences sharedPreferences;

    //Set fragment's TAG
    public static final String TAG = "login_fragment";

    private SimpleFacebook mSimpleFacebook;

    User user;

    @Bind(R.id.email_edittext)

    @NotEmpty
    @Email
    EditText emailET;

    Validator validator;
    @Bind(R.id.password_edittext)

    @NotEmpty
    EditText passwordET;
    String emailString, passwordString;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        emailET = (EditText) view.findViewById(R.id.email_edittext);
        passwordET = (EditText) view.findViewById(R.id.password_edittext);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Set toolbar title
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Login");

        sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);

        //Check if email and password are saved and set them
        emailET.setText(sharedPreferences.getString("email", ""));
        passwordET.setText(sharedPreferences.getString("password", ""));

        //validator will check errors
        validator = new Validator(this);
        validator.setValidationListener(this);

        //permissions for facebook
        Permission[] permissions = new Permission[]{
                Permission.USER_PHOTOS,
                Permission.EMAIL,
                Permission.PUBLISH_ACTION
        };

        //facebook configuration
        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getString(R.string.app_id))
                .setNamespace("com.tomasvitek.damitest")
                .setPermissions(permissions)
                .build();

        SimpleFacebook.setConfiguration(configuration);
    }

    //Facebook Login button click
    @OnClick(R.id.login_facebook_button)
    void onLoginFacebookClick() {
        mSimpleFacebook.login(onLoginListener);
    }

    //Login button click
    @OnClick(R.id.login_button)
    void onLoginClick() {
        //start validator
        validator.validate();
    }

    //Account button click
    @OnClick(R.id.account_button)
    void onUcetClick() {
        showFragment(LoginFragment.newInstance(), LoginFragment.TAG, false);
    }

    //Map button click
    @OnClick(R.id.map_button)
    void onMapaClick() {
        showFragment(MapsFragment.newInstance(), MapsFragment.TAG, false);
    }

    //login listener for facebook
    OnLoginListener onLoginListener = new OnLoginListener() {

        @Override
        public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
            Log.i("tag", "Logged in");
        }

        @Override
        public void onCancel() {
            Log.i("tag", "Cancel");
        }

        @Override
        public void onFail(String reason) {
            Log.i("tag", "Fail");
        }

        @Override
        public void onException(Throwable throwable) {
        }
    };

    //Fragment's newInstance method
    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onValidationSucceeded() {

        //get datas from edittexts
        emailString = emailET.getText().toString();
        passwordString = passwordET.getText().toString();

        //Put datas to parameters
        RequestParams params = new RequestParams();
        params.put("password", passwordString);
        params.put("email", emailString);

        MyHttpClient.post("login", params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {

                    Gson gson = new Gson();
                    user = gson.fromJson(response.getJSONObject("response").toString(), User.class);

                    //Save user class to MainActivity
                    ((MainActivity)getActivity()).setUser(user);

                    //Save login data to sharedprefs
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("email", emailString);
                    editor.putString("password", passwordString);
                    editor.apply();

                    //if Login was succesfull than show accountFragment
                    showFragment(AccountFragment.newInstance(),AccountFragment.TAG,false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
            }
        });


    }

    //If validation failed than show error
    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}

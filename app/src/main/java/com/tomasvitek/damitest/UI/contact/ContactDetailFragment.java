package com.tomasvitek.damitest.UI.contact;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.Contact;

import butterknife.Bind;

/**
 * Created by Tomáš Vítek on 29.03.2016 at 19:35.
 */
public class ContactDetailFragment extends BaseFragment {

    //Set fragment's TAG
    public static final String TAG = "contact_detail_fragment";

    //Define views and objects

    @Bind(R.id.image_detail) ImageView image;
    @Bind(R.id.titleTV) TextView name;
    @Bind(R.id.contact_number) TextView number;
    @Bind(R.id.contact_email) TextView email;

    Contact contact;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Enable icons in toolbar
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //Set fragment's view
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //Set toolbar title
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Contact detail");

        //More toolbar settings
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((MainActivity) getActivity()).getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back when arrow is clicked
                getFragmentManager().popBackStack();
            }
        });

        //get saved contact from MainActivity
        contact = ((MainActivity) getActivity()).contact;

        //Set imageLoader
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(contact.getPhoto(), image);

        //Display some infos about contact
        name.setText(contact.getName()+" "+contact.getLastname());
        number.setText(contact.getPhone());
        email.setText(contact.getEmail());

    }

    //Fragment's newInstance method
    public static ContactDetailFragment newInstance() {
        ContactDetailFragment fragment = new ContactDetailFragment();
        return fragment;
    }

    //Show Edit icon in toolbar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_contacts_detail, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Edit icon click
        if (id == R.id.edit) {
            showFragment(ContactEditFragment.newInstance(),ContactEditFragment.TAG,true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

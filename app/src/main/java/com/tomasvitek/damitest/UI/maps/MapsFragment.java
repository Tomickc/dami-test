package com.tomasvitek.damitest.UI.maps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.UI.login.LoginFragment;
import com.tomasvitek.damitest.base.BaseFragment;
import com.tomasvitek.damitest.models.MapMarker;

import java.util.List;

import butterknife.OnClick;

public class MapsFragment extends BaseFragment implements OnMapReadyCallback {

    //Set fragment's TAG
    public static final String TAG = "maps_fragment";

    GoogleMap mMap;
    ImageView image;
    List<MapMarker> points;

    private static View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_maps, container, false);
        } catch (InflateException e) {
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Set toolbar config
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);

        //Set map for fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //set toolbar title
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("MapMarker");
    }

    //Account button click
    @OnClick(R.id.account_button_map)
    void onUcetClick() {
        showFragment(LoginFragment.newInstance(), LoginFragment.TAG, false);
    }

    //Map button click
    @OnClick(R.id.map_button_map)
    void onMapaClick() {
        showFragment(MapsFragment.newInstance(), MapsFragment.TAG, false);
    }

    //Set map config
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;

        //Map zoom
        mMap.moveCamera(CameraUpdateFactory.zoomTo(2));

        //Add points from List of MapMarkers to map
        points = ((MainActivity) getActivity()).mapPointsList;
        for (int i = 0; i < points.size(); i++) {
            MapMarker mapMarker = points.get(i);
            addMarker(mapMarker.getLat(), mapMarker.getLng());
        }

        //Set pop-up window
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = getActivity().getLayoutInflater().inflate(R.layout.map_info, null);
                image = (ImageView) v.findViewById(R.id.place_image);


                for (int i = 0; i < points.size(); i++) {
                    MapMarker mapMarker = points.get(i);


                    //Check if clicked point is current mapMarker
                    if (marker.getPosition().equals(new LatLng(mapMarker.getLat(), mapMarker.getLng()))) {

                        //Save current mapMarker to MainActivity
                        ((MainActivity) getActivity()).setMapMarker(mapMarker);

                        //Set infoBitmap
                        image.setImageBitmap(mapMarker.getInfoViewBitmap());

                        //on Image click show markerDetailFragment
                        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                showFragment(MarkerDetailFragment.newInstance(), MarkerDetailFragment.TAG, true);
                            }
                        });
                    }
                }
                return v;
            }
        });
    }


    //Fragment's newInstance method
    public static MapsFragment newInstance() {
        MapsFragment fragment = new MapsFragment();
        return fragment;
    }

    //Method which will add new points into map
    public Marker addMarker(double lat, double lng) {
        return mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).icon((BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))));
    }

}

package com.tomasvitek.damitest.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.tomasvitek.damitest.MainActivity;
import com.tomasvitek.damitest.R;
import com.tomasvitek.damitest.UI.account.AccountFragment;
import com.tomasvitek.damitest.UI.contact.ContactsFragment;
import com.tomasvitek.damitest.UI.login.LoginFragment;
import com.tomasvitek.damitest.UI.maps.MapsLoggedFragment;

import butterknife.ButterKnife;

/**
 * Created by Tomáš Vítek on 08.03.2016 at 17:15.
 */

//Fragments will inherit methods from this BaseFragment
public class BaseFragment extends Fragment {

    Drawer drawerBuilder;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Set Butterknife
        ButterKnife.bind(this, view);
    }

    //show new fragment method
    public void showFragment(BaseFragment fragment, String tag, boolean backStack) {

        //create new transaction
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        //transaction will start new fragment
        transaction.replace(R.id.fragment, fragment, tag);

        //check if transaction has backstack
        if (backStack) {

            //backstack available
            transaction.addToBackStack(tag);
        }

        //commit transaction
        transaction.commit();
    }

    public void showNavigationDrawer(){
        drawerBuilder = new DrawerBuilder()
                .withActivity(getActivity())
                .withToolbar(((MainActivity) getActivity()).getToolbar())
                .addDrawerItems(
                        //Add some Items
                        new PrimaryDrawerItem().withName("Detail účtu"),
                        new PrimaryDrawerItem().withName("Kontakty"),
                        new PrimaryDrawerItem().withName("Mapa"),
                        new PrimaryDrawerItem().withName("Odhlásit")

                )
                //On item click
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch (position) {
                            case 0:
                                showFragment(AccountFragment.newInstance(), AccountFragment.TAG, false);
                                break;
                            case 1:
                                showFragment(ContactsFragment.newInstance(), ContactsFragment.TAG, false);
                                break;
                            case 2:
                                showFragment(MapsLoggedFragment.newInstance(), MapsLoggedFragment.TAG, false);
                                break;
                            case 3:
                                showFragment(LoginFragment.newInstance(), LoginFragment.TAG, false);
                                hideNavigationDrawer();
                                break;
                        }
                        return false;
                    }
                })
                .withSelectedItem(-1)
                .build();
    }

    public void hideNavigationDrawer(){
        drawerBuilder.removeAllItems();
    }
}
